// Copyright 2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include <unistd.h>

#include <chrono>
#include <librealsense2/rs.hpp>
#include <mutex>
#include <opencv2/opencv.hpp>
#include <thread>

#define PI_FL 3.141592f

struct float3 {
  float x, y, z;
  float3 operator*(float t) { return {x * t, y * t, z * t}; }

  void add(float t1, float t2, float t3) {
    x += t1;
    y += t2;
    z += t3;
  }
};

// This rotation estimator is from the rs-motion example
class rotation_estimator {
  // theta is the angle of camera rotation in x, y and z components
  float3 theta;
  std::mutex theta_mtx;
  /* alpha indicates the part that gyro and accelerometer take in computation
  of theta; higher alpha gives more weight to gyro, but too high
  values cause drift; lower alpha gives more weight to accelerometer,
  which is more sensitive to disturbances */
  float alpha = 0.98f;
  bool firstGyro = true;
  bool firstAccel = true;
  // Keeps the arrival time of previous gyro frame
  double last_ts_gyro = 0;

 public:
  // Function to calculate the change in angle of motion based on data from gyro
  void process_gyro(rs2_vector gyro_data, double ts) {
    if (firstGyro)  // On the first iteration, use only data from accelerometer
                    // to set the camera's initial position
    {
      firstGyro = false;
      last_ts_gyro = ts;
      return;
    }
    // Holds the change in angle, as calculated from gyro
    float3 gyro_angle;

    // Initialize gyro_angle with data from gyro
    gyro_angle.x = gyro_data.x;  // Pitch
    gyro_angle.y = gyro_data.y;  // Yaw
    gyro_angle.z = gyro_data.z;  // Roll

    // Compute the difference between arrival times of previous and current gyro
    // frames
    double dt_gyro = (ts - last_ts_gyro) / 1000.0;
    last_ts_gyro = ts;

    // Change in angle equals gyro measures * time passed since last measurement
    gyro_angle = gyro_angle * static_cast<float>(dt_gyro);

    // Apply the calculated change of angle to the current angle (theta)
    std::lock_guard<std::mutex> lock(theta_mtx);
    theta.add(-gyro_angle.z, -gyro_angle.y, gyro_angle.x);
  }

  void process_accel(rs2_vector accel_data) {
    // Holds the angle as calculated from accelerometer data
    float3 accel_angle;

    // Calculate rotation angle from accelerometer data
    accel_angle.z = atan2(accel_data.y, accel_data.z);
    accel_angle.x = atan2(accel_data.x, sqrt(accel_data.y * accel_data.y +
                                             accel_data.z * accel_data.z));

    // If it is the first iteration, set initial pose of camera according to
    // accelerometer data (note the different handling for Y axis)
    std::lock_guard<std::mutex> lock(theta_mtx);
    if (firstAccel) {
      firstAccel = false;
      theta = accel_angle;
      // Since we can't infer the angle around Y axis using accelerometer data,
      // we'll use PI as a convetion for the initial pose
      theta.y = PI_FL;
    } else {
      /*
      Apply Complementary Filter:
          - high-pass filter = theta * alpha:  allows short-duration signals to
      pass through while filtering out signals that are steady over time, is
      used to cancel out drift.
          - low-pass filter = accel * (1- alpha): lets through long term
      changes, filtering out short term fluctuations
      */
      theta.x = theta.x * alpha + accel_angle.x * (1 - alpha);
      theta.z = theta.z * alpha + accel_angle.z * (1 - alpha);
    }
  }

  // Returns the current rotation angle
  float3 get_theta() {
    std::lock_guard<std::mutex> lock(theta_mtx);
    return theta;
  }
};

int main(int argc, char* argv[]) try {
  rs2::pipeline pipe;
  rs2::config cfg;
  cfg.enable_stream(RS2_STREAM_ACCEL, RS2_FORMAT_MOTION_XYZ32F);
  cfg.enable_stream(RS2_STREAM_GYRO, RS2_FORMAT_MOTION_XYZ32F);
  cfg.enable_stream(RS2_STREAM_COLOR, RS2_FORMAT_BGR8);
  rotation_estimator algo;

  using namespace cv;
  const auto window_name = "Preview";
  namedWindow(window_name, WINDOW_AUTOSIZE);

  static float angle = 0;
  pipe.start(cfg, [&](rs2::frame frame) {
    if (frame.get_profile().stream_type() == RS2_STREAM_COLOR) {
      auto frames = frame.as<rs2::frameset>();
      rs2::video_frame color_frame = frames.get_color_frame();

      const int w = color_frame.as<rs2::video_frame>().get_width();
      const int h = color_frame.as<rs2::video_frame>().get_height();

      Mat image(Size(w, h), CV_8UC3, (void*)color_frame.get_data(),
                Mat::AUTO_STEP);
      Point center = Point(image.cols / 2, image.rows / 2);
      Mat rotation = getRotationMatrix2D(center, angle, 1.f);
      Mat rotated_image;
      warpAffine(image, rotated_image, rotation, image.size());
      angle = algo.get_theta().x * 180 / PI_FL;

      imshow(window_name, rotated_image);
      waitKey(1);
    } else if (frame && frame.get_profile().stream_type() == RS2_STREAM_GYRO &&
               frame.get_profile().format() == RS2_FORMAT_MOTION_XYZ32F) {
      auto motion = frame.as<rs2::motion_frame>();
      double ts = motion.get_timestamp();
      rs2_vector gyro_data = motion.get_motion_data();
      algo.process_gyro(gyro_data, ts);
    } else if (frame && frame.get_profile().stream_type() == RS2_STREAM_ACCEL &&
               frame.get_profile().format() == RS2_FORMAT_MOTION_XYZ32F) {
      auto motion = frame.as<rs2::motion_frame>();
      rs2_vector accel_data = motion.get_motion_data();
      algo.process_accel(accel_data);
    } else {
      std::cout << "Unexpected stream\n";
    }
  });

  waitKey(0);  // Let's sleep until a keypress for exit occurs
  // Or could just do this
  // while (true) std::this_thread::sleep_for(std::chrono::milliseconds(1000));

  return EXIT_SUCCESS;
} catch (const rs2::error& e) {
  std::cerr << "RealSense error calling " << e.get_failed_function() << "("
            << e.get_failed_args() << "):\n    " << e.what() << std::endl;
  return EXIT_FAILURE;
} catch (const std::exception& e) {
  std::cerr << e.what() << std::endl;
  return EXIT_FAILURE;
}
