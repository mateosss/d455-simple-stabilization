Simple video stabilization preview, uses bits from rs-motion realsense example.

# Compile and run

```
c++ stabilization.cpp -lrealsense2 `pkg-config opencv --libs --cflags`
./a.out
```

[![](http://img.youtube.com/vi/gHE4sAfRiiI/0.jpg)](http://www.youtube.com/watch?v=gHE4sAfRiiI "Demo of the example")
